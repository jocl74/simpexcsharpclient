﻿# Hi Simpex User! #

## Documentation # 
The complete documentation is available at:
[Simpex documentation](https://ws2.spedion.de/simpex/3.1/Docu.aspx)

## The namespace ##
    using SimpexLib.MessageService;

## setting up password every time from your own config ##
    MessageService svc = MessageService.Create("yourUser", "yourPassword");

## using user from config below ##
    MessageService msgSvc = MessageService.CreateWithCredentials();

But you have to setup the credentials in App.config or Web.config like this:

    <?xml version="1.0" encoding="utf-8"?>
    <configuration>
        <configSections>
          <sectionGroup name="applicationSettings" type="System.Configuration.ApplicationSettingsGroup, System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089">
            <section name="SimpexLib.Properties.Settings" type="System.Configuration.ClientSettingsSection, System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" requirePermission="false"/>
          </sectionGroup>
        </configSections>
        <startup> 
            <supportedRuntime version="v4.0" sku=".NETFramework,Version=v4.0"/>
        </startup>
        <applicationSettings>
          <SimpexLib.Properties.Settings>
            <setting name="SimpexLib_MessageService_MessageService" serializeAs="String">
              <value>https://ws2.spedion.de/simpex/3.1/Services/MessageService.asmx</value>
            </setting>
            <setting name="SimpexLib_User" serializeAs="String">
              <value>XXX - ENTER your user HERE - XXX</value>
            </setting>
            <setting name="SimpexLib_Password" serializeAs="String">
              <value>XXX - ENTER your password HERE - XXX</value>
            </setting>
          </SimpexLib.Properties.Settings>
        </applicationSettings>
    </configuration>

## Example usage ##
    int count = msgSvc.GetMessageCount();
    string version msgSvc.GetVersion();