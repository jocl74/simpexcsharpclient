﻿using SimpexLib.MessageService;
using System;

namespace SimpexCSharpClient
{
    class Program
    {
        static void Main()
        {
            using (var ms = MessageService.CreateWithCredentials())
            {
                Console.WriteLine(ms.GetVersion());
                var msg = new Message
                {
                    Form = 1501,
                    Vehicle = "13579",
                    MessageTimeUtc = DateTime.UtcNow,
                    Text = "Hallo! Das ist eine Test-Textmeldung!"
                };
                var result = ms.AddMessage(msg);
                Console.WriteLine(result.Description);
                Console.WriteLine(ms.GetVersion());
            }
            Console.WriteLine("press any key to exit.");
            Console.ReadKey();
        }
    }
}
